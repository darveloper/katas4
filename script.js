const gotCitiesCSV =
  "King's Landing, Braavos, Volantis, Old Valyria, Free Cities, Qarth, Meereen";
const lotrCitiesArray = [
  "Mordor",
  "Gondor",
  "Rohan",
  "Beleriand",
  "Mirkwood",
  "Dead Marshes",
  "Rhun",
  "Harad"
];
const bestThing =
  "The best thing about a boolean is even if you are wrong you are only off by a bit";

//KATA 1

let header = document.createElement("div");
header.textContent = "Kata 1";
document.body.appendChild(header);

function kata1() {
  let newElement = document.createElement("div");
  newElement.textContent = gotCitiesCSV.split(", ");

  document.body.appendChild(newElement);
  return gotCitiesCSV; // Don't forget to return your output!
}
kata1();
//

//KATA2
let header2 = document.createElement("div");
header2.textContent = "Kata 2";
document.body.appendChild(header2);

function kata2() {
  let newElement = document.createElement("div");
  newElement.textContent = bestThing.split(" ");

  document.body.appendChild(newElement);
  return bestThing; // Don't forget to return your output!
}
kata2();
//

//KATA3
let header3 = document.createElement("div");
header3.textContent = "Kata 3";
document.body.appendChild(header3);

function kata3() {
  let newElement = document.createElement("div");
  newElement.textContent = gotCitiesCSV.replace(/,/g, ";");

  document.body.appendChild(newElement);
  return gotCitiesCSV; // Don't forget to return your output!
}
kata3();
//

//KATA4
let header4 = document.createElement("div");
header4.textContent = "Kata 4";
document.body.appendChild(header4);

function kata4() {
  let newElement = document.createElement("div");
  newElement.textContent = lotrCitiesArray.toString();

  document.body.appendChild(newElement);
  return lotrCitiesArray; // Don't forget to return your output!
}
kata4();
//

//KATA5
let header5 = document.createElement("div");
header5.textContent = "Kata 5";
document.body.appendChild(header5);

function kata5() {
  let newElement = document.createElement("div");
  let newLot = [
    "Mordor",
    "Gondor",
    "Rohan",
    "Beleriand",
    "Mirkwood",
    "Dead Marshes",
    "Rhun",
    "Harad"
  ];
  delete newLot[5];
  delete newLot[6];
  delete newLot[7];
  newElement.textContent = newLot;

  document.body.appendChild(newElement);
  return newLot; // Don't forget to return your output!
}
kata5();
//

//KATAS6
let header6 = document.createElement("div");
header6.textContent = "Kata 6";
document.body.appendChild(header6);

function kata6() {
  let newElement = document.createElement("div");
  let newLot1 = [
    "Mordor",
    "Gondor",
    "Rohan",
    "Beleriand",
    "Mirkwood",
    "Dead Marshes",
    "Rhun",
    "Harad"
  ];
  delete newLot1[0];
  delete newLot1[1];
  delete newLot1[2];
  delete newLot1[3];
  delete newLot1[4];
  newElement.textContent = newLot1;

  document.body.appendChild(newElement);
  return newLot1; // Don't forget to return your output!
}
kata6();

//KATA7
let header7 = document.createElement("div");
header7.textContent = "Kata 7";
document.body.appendChild(header7);

function kata7() {
  let newElement = document.createElement("div");

  newElement.textContent = lotrCitiesArray.slice(3, 5);

  document.body.appendChild(newElement);
  return newElement; // Don't forget to return your output!
}
kata7();
//

//KATA8
let header8 = document.createElement("div");
header8.textContent = "Kata 8";
document.body.appendChild(header8);

function kata8() {
  let newElement = document.createElement("div");
  let newLot2 = [
    "Mordor",
    "Gondor",
    "Rohan",
    "Beleriand",
    "Mirkwood",
    "Dead Marshes",
    "Rhun",
    "Harad"
  ];
  newLot2.splice(2, 1);
  newElement.textContent = newLot2;

  document.body.appendChild(newElement);
  return newElement; // Don't forget to return your output!
}
kata8();
//

//KATA9

let header9 = document.createElement("div");
header9.textContent = "Kata 9";
document.body.appendChild(header9);

function kata9() {
  let newElement = document.createElement("div");

  lotrCitiesArray.splice(6, 2);
  newElement.textContent = lotrCitiesArray;

  document.body.appendChild(newElement);
  return lotrCitiesArray; // Don't forget to return your output!
}
kata9();
//

//KATA10
let header10 = document.createElement("div");
header10.textContent = "Kata 10";
document.body.appendChild(header10);

function kata10() {
  let newElement = document.createElement("div");

  lotrCitiesArray.splice(0, "Rojan");
  newElement.textContent = lotrCitiesArray;

  document.body.appendChild(newElement);
  return lotrCitiesArray; // Don't forget to return your output!
}
kata10();
//

//KATA11
let header11 = document.createElement("div");
header11.textContent = "Kata 11";
document.body.appendChild(header11);

function kata11() {
  let newElement = document.createElement("div");

  lotrCitiesArray.splice(5, 1, "Deadest Marshes");
  newElement.textContent = lotrCitiesArray;

  document.body.appendChild(newElement);
  return lotrCitiesArray; // Don't forget to return your output!
}
kata11();
//

//KATA12
let header12 = document.createElement("div");
header12.textContent = "Kata 12";
document.body.appendChild(header12);

function kata12() {
  let newElement = document.createElement("div");

  newElement.textContent = bestThing.slice(0, 14);

  document.body.appendChild(newElement);
  return newElement; // Don't forget to return your output!
}
kata12();
//

//KATA13
let header13 = document.createElement("div");
header13.textContent = "Kata 13";
document.body.appendChild(header13);

function kata13() {
  let newElement = document.createElement("div");

  newElement.textContent = bestThing.slice(-12);

  document.body.appendChild(newElement);
  return newElement; // Don't forget to return your output!
}
kata13();
//

//KATA14
let header14 = document.createElement("div");
header14.textContent = "Kata 14";
document.body.appendChild(header14);

function kata14() {
  let newElement = document.createElement("div");

  newElement.textContent = bestThing.slice(23, 38);

  document.body.appendChild(newElement);
  return newElement; // Don't forget to return your output!
}
kata14();

//

//KATA15
let header15 = document.createElement("div");
header15.textContent = "Kata 15";
document.body.appendChild(header15);

function kata15() {
  let newElement = document.createElement("div");

  newElement.textContent = bestThing.substring(
    bestThing.length - 12,
    bestThing.length
  );

  document.body.appendChild(newElement);
  return newElement; // Don't forget to return your output!
}
kata15();
//

//KATA16
let header16 = document.createElement("div");
header16.textContent = "Kata 16";
document.body.appendChild(header16);

function kata16() {
  let newElement = document.createElement("div");

  newElement.textContent = bestThing.substring(23, 38);

  document.body.appendChild(newElement);
  return newElement; // Don't forget to return your output!
}
kata16();
//

//KATA17
let header17 = document.createElement("div");
header17.textContent = "Kata 17";
document.body.appendChild(header17);

function kata17() {
  let newElement = document.createElement("div");

  newElement.textContent = bestThing.indexOf("only");

  document.body.appendChild(newElement);
  return newElement; // Don't forget to return your output!
}
kata17();
//

//KATA18
let header18 = document.createElement("div");
header18.textContent = "Kata 18";
document.body.appendChild(header18);

function kata18() {
  let newElement = document.createElement("div");

  var n = bestThing.split(" ");
  newElement.textContent = n.pop();

  document.body.appendChild(newElement);
  return newElement; // Don't forget to return your output!
}
kata18();
//

//KATA19
let header19 = document.createElement("div");
header19.textContent = "Kata 19";
document.body.appendChild(header19);

function kata19() {
  doubles = [];
  var arr = gotCitiesCSV.split(",");
  arr.forEach(word => {
    if (
      word.includes("ee") ||
      word.includes("aa") ||
      word.includes("ii") ||
      word.includes("oo") ||
      word.includes("uu")
    ) {
      doubles.push(word);
    }
  });

  let newElement = document.createElement("div");
  newElement.textContent = doubles;
  document.body.appendChild(newElement);
  return newElement;
}
kata19();

//

//KATA20
let header20 = document.createElement("div");
header20.textContent = "Kata 20";
document.body.appendChild(header20);

function kata20() {
  or = [];
  lotrCitiesArray.forEach(word => {
    if (word.endsWith("or")) {
      or.push(word);
    }
  });

  let newElement = document.createElement("div");
  newElement.textContent = or;
  document.body.appendChild(newElement);
  return newElement;
}
kata20();
//

//KATA21
let header21 = document.createElement("div");
header21.textContent = "Kata 21";
document.body.appendChild(header21);

function kata21() {
  bWords = [];
  var arr = bestThing.split(" ");

  arr.forEach(word => {
    if (word.startsWith("b")) {
      bWords.push(word);
    }
  });

  let newElement = document.createElement("div");
  newElement.textContent = bWords;
  document.body.appendChild(newElement);
  return newElement;
}
kata21();
//

//KATA22
let header22 = document.createElement("div");
header22.textContent = "Kata 22";
document.body.appendChild(header22);

function kata22() {
  if (lotrCitiesArray.includes("Mirkwood")) {
    let newElement = document.createElement("div");
    newElement.textContent = "yes";
    document.body.appendChild(newElement);
    return newElement;
  } else {
    let newElement = document.createElement("div");
    newElement.textContent = "no";
    document.body.appendChild(newElement);
    return newElement;
  }
}
kata22();
//

//KATA23
let header23 = document.createElement("div");
header23.textContent = "Kata 23";
document.body.appendChild(header23);

function kata23() {
  if (lotrCitiesArray.includes("Hollywood")) {
    let newElement = document.createElement("div");
    newElement.textContent = "yes";
    document.body.appendChild(newElement);
    return newElement;
  } else {
    let newElement = document.createElement("div");
    newElement.textContent = "no";
    document.body.appendChild(newElement);
    return newElement;
  }
}
kata23();
//

//KATA24
let header24 = document.createElement("div");
header24.textContent = "Kata 24";
document.body.appendChild(header24);

function kata24() {
  var i = lotrCitiesArray.indexOf("Mirkwood");
  let newElement = document.createElement("div");
  newElement.textContent = "The index of Mikrwood is : " + i;
  document.body.appendChild(newElement);
}
kata24();
//

//KATA25
let header25 = document.createElement("div");
header25.textContent = "Kata 25";
document.body.appendChild(header25);

function kata25() {
  lotrCitiesArray.forEach(phrase => {
    if (phrase.includes(" ")) {
      let newElement = document.createElement("div");
      newElement.textContent = phrase;
      document.body.appendChild(newElement);
    }
  });
}
kata25();
//

//KATA26
let header26 = document.createElement("div");
header26.textContent = "Kata 26";
document.body.appendChild(header26);

function kata26() {
  var r = lotrCitiesArray.reverse();
  let newElement = document.createElement("div");
  newElement.textContent = r;
  document.body.appendChild(newElement);
}
kata26();
//

//KATA27
let header27 = document.createElement("div");
header27.textContent = "Kata 27";
document.body.appendChild(header27);

function kata27() {
  var alph = lotrCitiesArray.sort();
  let newElement = document.createElement("div");
  newElement.textContent = alph;
  document.body.appendChild(newElement);
}
kata27();
//

//KATA28
let header28 = document.createElement("div");
header28.textContent = "Kata 28";
document.body.appendChild(header28);

lotrCitiesArray.sort(function(a, b) {
  return a.length - b.length;
});
document.writeln(lotrCitiesArray);
//

//KATA29
let header29 = document.createElement("div");
header29.textContent = "Kata 29";
document.body.appendChild(header29);
var pop = lotrCitiesArray.pop();
function kata29() {
  let newElement = document.createElement("div");
  newElement.textContent = lotrCitiesArray;
  document.body.appendChild(newElement);
}
kata29();

//

//KATA30
let header30 = document.createElement("div");
header30.textContent = "Kata 30";
document.body.appendChild(header30);
var push = lotrCitiesArray.push(pop);
function kata30() {
  let newElement = document.createElement("div");
  newElement.textContent = lotrCitiesArray;
  document.body.appendChild(newElement);
}
kata30();
//

//KATA31
let header31 = document.createElement("div");
header31.textContent = "Kata 31";
document.body.appendChild(header31);

var shift = lotrCitiesArray.shift();
function kata31() {
  let newElement = document.createElement("div");
  newElement.textContent = lotrCitiesArray;
  document.body.appendChild(newElement);
}
kata31();
//

//KATA32
let header32 = document.createElement("div");
header32.textContent = "Kata 32";
document.body.appendChild(header30);
var unshift = lotrCitiesArray.unshift(shift);
function kata32() {
  let newElement = document.createElement("div");
  newElement.textContent = lotrCitiesArray;
  document.body.appendChild(newElement);
}
kata32();